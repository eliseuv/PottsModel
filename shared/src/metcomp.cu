#include "metcomp.cuh"

/******************************************/
/*    Computational Methods for Physics   */
/*            CUDA Capabilities           */
/******************************************/

// CUDA query device
void CUDA_queryDevices(void)
{
  printf("\n--- Query CUDA Devices ---\n");
  int gpu_count;
  cudaDeviceProp gpu_prop;

  cudaGetDeviceCount(&gpu_count);
  printf("Number of GPUs found: %d\n", gpu_count);

  for (size_t i = 0; i < gpu_count; i++)
  {
    cudaGetDeviceProperties(&gpu_prop, i);
    printf("\n--- Device %d properties ---\n", i);
    printf("Name: %s\n", gpu_prop.name);
    printf("Clock rate: %fGHz\n", gpu_prop.clockRate/1000000.0);
    printf("Global memory: %fMB\n", gpu_prop.totalGlobalMem/(1024.0*1024.0));
    printf("Compute capability: %d.%d\n", gpu_prop.major, gpu_prop.minor);
    printf("Multiprocessor count: %d\n", gpu_prop.multiProcessorCount);
    printf( "Shared mem per MP: %fkB\n", gpu_prop.sharedMemPerBlock/1024.0);
    printf( "Registers per MP: %d\n", gpu_prop.regsPerBlock);
    printf( "Threads in warp: %d\n", gpu_prop.warpSize);
    printf( "Max threads per block: %d\n", gpu_prop.maxThreadsPerBlock);
    printf( "Max Block dimension: (%d, %d, %d)\n", gpu_prop.maxThreadsDim[0], gpu_prop.maxThreadsDim[1], gpu_prop.maxThreadsDim[2]);
    printf( "Max Grid dimension: (%d, %d, %d)\n", gpu_prop.maxGridSize[0], gpu_prop.maxGridSize[1], gpu_prop.maxGridSize[2]);
    printf("\n");
  }
}

// CUDA query device
void CUDA_queryDevices(std::string filename)
{
  std::ofstream file;
  file.open(filename.c_str());

  file << "--- Query CUDA Devices ---\n";

  int gpu_count;
  cudaDeviceProp gpu_prop;

  cudaGetDeviceCount(&gpu_count);
  file << "Number of GPUs found: " << gpu_count << '\n';

  for (size_t i = 0; i < gpu_count; i++)
  {
    cudaGetDeviceProperties(&gpu_prop, i);
    file << "\n--- Device" << i << " properties ---\n";
    file << "Name: " << gpu_prop.name << '\n';
    file << "Clock rate: " << gpu_prop.clockRate/1000000.0 << " GHz\n";
    file << "Global memory: " << gpu_prop.totalGlobalMem/(1024.0*1024.0) << "%fMB\n";
    file << "Compute capability: " << gpu_prop.major << '.' << gpu_prop.minor << '\n';
    file << "Multiprocessor count: " << gpu_prop.multiProcessorCount << '\n';
    file << "Shared mem per MP: " << gpu_prop.sharedMemPerBlock/1024.0 << "kB\n";
    file << "Registers per MP: " << gpu_prop.regsPerBlock << '\n';
    file << "Threads in warp: " << gpu_prop.warpSize << '\n';
    file << "Max threads per block: " << gpu_prop.maxThreadsPerBlock << '\n';
    file << "Max Block dimension: (" << gpu_prop.maxThreadsDim[0] << ", " << gpu_prop.maxThreadsDim[1] << ", " << gpu_prop.maxThreadsDim[2] << ")\n";
    file << "Max Grid dimension: (" << gpu_prop.maxGridSize[0] << ", " << gpu_prop.maxGridSize[1] << ", " << gpu_prop.maxGridSize[2] << ")\n";
    file << printf("\n");
  }

  file.close();
}
