#pragma once

/******************************************/
/*    Computational Methods for Physics   */
/*            CUDA Capabilities           */
/******************************************/

#include <cstdio>
#include <string>
#include <fstream>
#include <cuda_runtime.h>

// CUDA query devices
void CUDA_queryDevices(void);
void CUDA_queryDevices(std::string filename);
