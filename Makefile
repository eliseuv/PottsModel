# Where is it compiled HOME/CLUSTER
LOCAL := CLUSTER

# System and Simulation to be compiled
SYS := PottsLongRange
SIM := MetMCEvo

# C Compiler
COMPILER := g++
CCFLAGS := -std=c++11 #-O3
# CUDA compiler
CUDA_COMPILER := nvcc
CUDA_FLAGS := -std=c++11 #-O3
# Linker
LINKER := $(CUDA_COMPILER)
LDFLAGS := $(CUDA_FLAGS)

# CUDA path
ifeq ($(LOCAL), HOME)
	CUDA_PATH := /opt/cuda
else ifeq ($(LOCAL), CLUSTER)
	CUDA_PATH := /opt/cuda-10.0
endif

# Libraries
CUDA_INCS := -I$(CUDA_PATH)/include
CUDA_LIBS := -L$(CUDA_PATH)/lib64 -lcudart -lcurand

# Execution
ifeq ($(LOCAL), HOME)
	CUDA_EXEC := nvprof
else ifeq ($(LOCAL), CLUSTER)
	CUDA_EXEC := nvprof
endif

#########################################################################

# Shared path
SHARED_SRC_FILES := $(wildcard ./shared/src/*.cpp) $(wildcard ./shared/src/*.cu)
SHARED_OBJ_FILES := $(SHARED_SRC_FILES)
SHARED_OBJ_FILES := $(patsubst ./shared/src/%.cpp, ./shared/obj/%.o, $(SHARED_OBJ_FILES))
SHARED_OBJ_FILES := $(patsubst ./shared/src/%.cu, ./shared/obj/%.o, $(SHARED_OBJ_FILES))

SHARED_INCS := $(CUDA_INCS) -I./shared/inc
SHARED_LIBS := $(CUDA_LIBS)

# System paths
SYS_SRC_FILES := $(wildcard ./sys/$(SYS)/src/*.cpp) $(wildcard ./sys/$(SYS)/src/*.cu)
SYS_OBJ_FILES := $(SYS_SRC_FILES)
SYS_OBJ_FILES := $(patsubst ./sys/$(SYS)/src/%.cpp, ./sys/$(SYS)/obj/%.o, $(SYS_OBJ_FILES))
SYS_OBJ_FILES := $(patsubst ./sys/$(SYS)/src/%.cu, ./sys/$(SYS)/obj/%.o, $(SYS_OBJ_FILES))

SYS_INCS  := $(SHARED_INCS) -I./sys/$(SYS)/inc
SYS_LIBS := $(SHARED_LIBS)

# Simulation paths
SIM_SRC_FILES := $(wildcard ./sim/$(SIM)/src/*.cpp) $(wildcard ./sim/$(SIM)/src/*.cu)
SIM_OBJ_FILES := $(SIM_SRC_FILES)
SIM_OBJ_FILES := $(patsubst ./sim/$(SIM)/src/%.cpp, ./sim/$(SIM)/obj/%.o, $(SIM_OBJ_FILES))
SIM_OBJ_FILES := $(patsubst ./sim/$(SIM)/src/%.cu, ./sim/$(SIM)/obj/%.o, $(SIM_OBJ_FILES))

SIM_INCS  := $(SYS_INCS) -I./sim/$(SIM)/inc
SIM_LIBS := $(SYS_LIBS)

OBJ_FILES := $(SHARED_OBJ_FILES) $(SYS_OBJ_FILES) $(SIM_OBJ_FILES)

#########################################################################
# Shared objects rule
./shared/obj/%.o: ./shared/src/%.cpp
	$(COMPILER) -c $< -o $@ $(CCFLAGS) $(SHARED_INCS) $(SHARED_LIBS)
./shared/obj/%.o: ./shared/src/%.cu
	$(CUDA_COMPILER) -c $< -o $@ $(CUDA_FLAGS) $(SHARED_INCS) $(SHARED_LIBS)

# System objects rule
./sys/$(SYS)/obj/%.o: ./sys/$(SYS)/src/%.cpp
	$(COMPILER) -c $< -o $@ $(CCFLAGS) $(SYS_INCS) $(SYS_LIBS)
./sys/$(SYS)/obj/%.o: ./sys/$(SYS)/src/%.cu
	$(CUDA_COMPILER) -c $< -o $@ $(CUDA_FLAGS) $(SYS_INCS) $(SYS_LIBS)

# Simulation objects rule
./sim/$(SIM)/obj/%.o: ./sim/$(SIM)/src/%.cpp
	$(CUDA_COMPILER) -c $< -o $@ $(CUDA_FLAGS) $(SIM_INCS) $(SIM_LIBS)
./sim/$(SIM)/obj/%.o: ./sim/$(SIM)/src/%.cu
	$(CUDA_COMPILER) -c $< -o $@ $(CUDA_FLAGS) $(SIM_INCS) $(SIM_LIBS)

#########################################################################

all: cuda_run clean

dirs:
	mkdir -p ./shared/obj ./sys/$(SYS)/obj ./sim/$(SIM)/obj ./sim/$(SIM)/bin

cuda_build: dirs $(OBJ_FILES)
	$(LINKER) $(OBJ_FILES) $(LDFLAGS) $(CUDA_LIBS) -o ./sim/$(SIM)/bin/$(SIM)_cuda

cuda_run: cuda_build
	$(CUDA_EXEC) ./sim/$(SIM)/bin/$(SIM)_cuda

clean:
	rm -rf ./shared/obj ./sys/$(SYS)/obj ./sim/$(SIM)/obj ./sim/$(SIM)/bin
