#include "metcomp.h"
#include "timer.h"
#include "metcomp.cuh"
#include "PottsLR2D.cuh"

int main()
{

  //****************
  // Configuration *
  //****************

  // Get info on GPUs
  CUDA_queryDevices();

  // Create timer
  Timer<double> timer;

  // Set parameters
  unint L = 1<<5, q = 2, s0 = 1, MCsteps = 1<<9;
  float T = 1.0, J = 1.0, sig = 1.0;
  bool T0inf = true;

  //*************
  // Simulation *
  //*************

  // Create system
  timer.start();

  PottsLR2D<double> sys(L, L, q, T, J, sig, T0inf, s0);

  timer.stop();
  timer.print("\nConstructor");

  // Print initial state
  sys.print();
  sys.print_grid();

  // MCMC Evolution
  timer.start();

  sys.MC_Metropolis_cpu(MCsteps);

  timer.stop();
  timer.print("\nMetropolis");

  // Print final state
  sys.print();
  sys.print_grid();

  return 0;

}
